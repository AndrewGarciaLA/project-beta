import React, { useEffect, useState } from 'react';

function CreateModelForm() {
    const [model, setModel] = useState('');
    const [pictureURL, setPictureURL] = useState('');
    const [manufacturer, setManufacturer] = useState([]);
    const [selectedManufacturer, setSelectedManufacturer] = useState('');

    const handleModelChange = (event) => {
        const value = event.target.value;
        setModel(value);
    }

    const handlePictureURLChange = (event) => {
        const value = event.target.value;
        setPictureURL(value);
    }

    const handleSelectedManufacturer = (event) => {
        const value = event.target.value;
        setSelectedManufacturer(value);
    }



    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.name = model;
        data.picture_url = pictureURL;
        data.manufacturer_id = parseInt(selectedManufacturer);

        const modelURL = 'http://localhost:8100/api/models/'
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(modelURL, fetchConfig);
        if (response.ok) {
            const newModel = await response.json();
            console.log(newModel);

            setModel('');
            setPictureURL('');
            setSelectedManufacturer('');
        }
    }

    const fetchManufacturers = async () => {
        const response = await fetch('http://localhost:8100/api/manufacturers/');

        if (response.ok) {
            const data = await response.json();
            setManufacturer(data.manufacturers);
        }
    }

    useEffect(() => {
        fetchManufacturers();
    }, []);

    return(
        <div className="container">
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Create a Vehicle Model</h1>
                        <form onSubmit={handleSubmit} id="create-model-form">
                        <div className="form-floating mb-3">
                                <input onChange={handleModelChange} placeholder="model" value={model} required type="text" id="model" name="model" className="form-control" />
                                <label htmlFor="model">Model name...</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={handlePictureURLChange} placeholder="pictureURL" value={pictureURL} required type="text" id="pictureURL" name="pictureURL" className="form-control" />
                                <label htmlFor="pictureURL">Picture URL...</label>
                            </div>
                            <div className="mb-3">
                            <label htmlFor="manufacturer" className="form-label">Manufacturer</label>
                            <select onChange={handleSelectedManufacturer} required id="selectedManufacturer" value={selectedManufacturer} name="selectedManufacturer" className="form-select">
                                <option defaultValue="">Choose a manufacturer...</option>
                                {manufacturer.map(manu => {
                                    return (
                                        <option key={manu.id} value={manu.id}>
                                            {manu.name}
                                        </option>
                                    )
                                })}
                            </select>
                        </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    )
}


    export default CreateModelForm;
