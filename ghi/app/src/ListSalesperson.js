import React, { useEffect, useState  } from 'react';


function ListSalesperson() {
    const [salespeople, setSalesPeople] = useState([]);

    const fetchData = async () => {
        const response = await fetch("http://localhost:8090/api/salespeople/")

        if (response.ok) {
            const data = await response.json();

            const sortedSalespeople = [...data.salespeople].sort((a, b) => {
                const nameA = a.first_name.toUpperCase();
                const nameB = b.first_name.toUpperCase();
                if (nameA < nameB) {
                    return -1;
                }
                if (nameA > nameB) {
                    return 1;
                }
                return 0;
            });
            setSalesPeople(sortedSalespeople);
        }
    };

    useEffect(() => {
        fetchData();
    }, []);


    return (
        <>
            <h1>Salesperson List</h1>
            <table className='table table-striped'>
                <thead>
                    <tr>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Employee Id</th>
                    </tr>
                </thead>
                <tbody>
                    {salespeople.map(salesPerson => {
                        return (
                            <tr key={salesPerson.id} value={salesPerson.href}>
                                <td>{salesPerson.first_name}</td>
                                <td>{salesPerson.last_name}</td>
                                <td>{salesPerson.employee_id}</td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        </>
    )
}

export default ListSalesperson;
