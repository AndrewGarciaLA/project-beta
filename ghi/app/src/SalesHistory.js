import React, { useEffect, useState } from 'react';

function SalesHistory() {
    const [sales, setSales] = useState ([]);
    const [selectedSalesperson, setSelectedSalesperson] = useState ('');
    const [salesperson, setSalesperson] = useState ([]);

    const handleSelectedSalesperson =(event) => {
        const value = event.target.value;
        setSelectedSalesperson(value);
    }



    const fetchSalesperson = async () => {
        const response = await fetch('http://localhost:8090/api/salespeople/');
        if (response.ok) {
            const data = await response.json();
            setSalesperson(data.salespeople);
        }
    }

    const fetchSales = async () => {
        const response = await fetch('http://localhost:8090/api/sales/');
        if (response.ok) {
            const data = await response.json();
            const filteredSales = data.sales.filter(sale => sale.salesperson.id === parseInt(selectedSalesperson))
            setSales(filteredSales);
        }
    }


    useEffect(() => {
        fetchSales();
        fetchSalesperson();
    }, [selectedSalesperson]);



return (
    <>

    <h1>Salesperson History</h1>
    <div className="form-floating mb-3">
        <select onChange={handleSelectedSalesperson} required id="selectedSalesperson" value={selectedSalesperson} name="selectedSalesperson" className="form-select">
            <option defaultValue="">Choose a salesperson...</option>
            {salesperson.map(sp => {
                return (
                    <option key={sp.id} value={sp.id}>
                        {sp.first_name} {sp.last_name}
                    </option>
                )
            })}
        </select>
    </div>
    <h2>Sales History:</h2>
    <table className='table table-striped'>
        <thead>
            <tr>
                <th>Salesperson Name</th>
                <th>Customer</th>
                <th>VIN</th>
                <th>Price</th>
            </tr>
        </thead>
        <tbody>
            {sales.map((sale) => {
                return (
                    <tr key={sale.id}>
                        <td>{sale.salesperson.first_name} {sale.salesperson.last_name}</td>
                        <td>{sale.customer.first_name} {sale.customer.last_name}</td>
                        <td>{sale.automobile.vin}</td>
                        <td>${sale.price}</td>
                    </tr>
                )
            })}
        </tbody>
    </table>
</>
)

}


export default SalesHistory;
