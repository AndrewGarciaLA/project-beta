import React, { useEffect, useState  } from 'react';
function ListManufacturers() {
    const [manufacturers, setManufacturers] = useState([]);

    const fetchManufacturers = async () => {
        const response = await fetch("http://localhost:8100/api/manufacturers/")

        if (response.ok) {
            const data = await response.json();

            const sortedManufacturers = [...data.manufacturers].sort((a, b) => {
                const nameA = a.name.toUpperCase();
                const nameB = b.name.toUpperCase();
                if (nameA < nameB) {
                    return -1;
                }
                if (nameA > nameB) {
                    return 1;
                }
                return 0;
            });
            setManufacturers(sortedManufacturers);
        }
    };

    useEffect(() => {
        fetchManufacturers();
    }, []);


    return (
        <>
            <h1>Manufacturer List</h1>
            <table className='table table-striped'>
                <thead>
                    <tr>
                        <th>Manufacturer Name</th>
                    </tr>
                </thead>
                <tbody>
                    {manufacturers.map(manufacturer => {
                        return (
                            <tr key={manufacturer.id} value={manufacturer.href}>
                                <td>{manufacturer.name}</td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        </>
    )
}

export default ListManufacturers;
