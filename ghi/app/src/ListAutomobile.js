import React, { useEffect, useState  } from 'react';


function ListAutomobiles() {
    const [automobiles, setAutomobiles] = useState([]);


    const fetchAutomobiles = async () => {
        const response = await fetch("http://localhost:8100/api/automobiles/")

        if (response.ok) {
            const data = await response.json();
            setAutomobiles(data.autos);
        }
    }


    useEffect(() => {
        fetchAutomobiles();
    }, []);


    return (
        <>
            <h1>Automobiles</h1>
            <table className='table table-striped'>
                <thead>
                    <tr>
                        <th>VIN</th>
                        <th>Color</th>
                        <th>Year</th>
                        <th>Model</th>
                        <th>Manufacturer</th>
                        <th>Sold</th>
                    </tr>
                </thead>
                <tbody>
                    {automobiles.map(autos => {
                        return (
                            <tr key={autos.id} value={autos.id}>
                                <td>{autos.vin}</td>
                                <td>{autos.color}</td>
                                <td>{autos.year}</td>
                                <td>{autos.model.name}</td>
                                <td>{autos.model.manufacturer.name}</td>
                                <td>{autos.sold ? "Yes" : "No"}</td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        </>
    )
}

export default ListAutomobiles;
