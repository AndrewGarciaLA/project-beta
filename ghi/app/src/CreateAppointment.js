import React, { useState, useEffect } from 'react';

function CreateAppointment() {
  const [vin, setVin] = useState('');
  const [date, setDate] = useState('');
  const [time, setTime] = useState('');
  const [reason, setReason] = useState('');
  const [status, setStatus] = useState('');
  const [customer, setCustomer] = useState('');
  const [SelectedTechnician, setSelectedTechnician] = useState('');
  const [technician, setTechnician] = useState([]);

  const handleVinChange = (event) => {
        const value = event.target.value;
        setVin(value);
    }

    const handleSelectedDate = (event) => {
        const value = event.target.value;
        setDate(value);
    }

    const handleSelectedTime = (event) => {
        const value = event.target.value;
        setTime(value);
    }

    const handleSelectedReason = (event) => {
        const value = event.target.value;
        setReason(value);
    }

    const handleSelectedStatus = (event) => {
        const value = event.target.value;
        setStatus(value);
    }

    const handleSelectedCustomer = (event) => {
        const value = event.target.value;
        setCustomer(value);
    }

    const handleSelectedTechnician = (event) => {
        const value = event.target.value;
        setSelectedTechnician(value);
    }

    const parseDateTime = (date_time) => {
        const [parseDate, parseTime] = date_time.split('T');
        date = parseDate
        time = parseTime
    }


    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};

        data.automobile = vin;
        data.date = parseDateTime.date;
        data.time = parseDateTime.time;
        data.reason = reason;
        data.status = status;
        data.customer = customer;
        data.technician = SelectedTechnician;
        console.log(data)


        const appointmentURL = 'http://localhost:8080/api/appointments/'
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
            const response = await fetch(appointmentURL, fetchConfig);
            if(response.ok){
                const newAppointment = await response.json();
                console.log(newAppointment);

                setVin('');
                setDate('');
                setTime('');
                setReason('');
                setStatus('');
                setCustomer('');
                setTechnician('');
            }
        }


    const fetchTechnician = async () => {
        const response = await fetch('http://localhost:8080/api/technicians/');
        console.log(response)

        if (response.ok) {
            const data = await response.json();
            setTechnician(data.technician);
        }
    }

        useEffect(() => {
            fetchTechnician();
        }, []);

    return(
        <div className="container">
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>New Appointment</h1>
                        <form onSubmit={handleSubmit} id="create-sales-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleVinChange} placeholder="vin" value={vin} required type="text" id="vin" name="vin" className="form-control" />
                            <label htmlFor="vin">VIN:</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleSelectedDate} placeholder="date" value={date} required type="date" id="date" name="date" className="form-control" />
                            <label htmlFor="date">Date:</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleSelectedTime} placeholder="time" value={time} required type="time" id="time" name="time" className="form-control" />
                            <label htmlFor="time">Time:</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleSelectedReason} placeholder="reason" value={reason} required type="text" id="reason" name="reason" className="form-control" />
                            <label htmlFor="reason">Reason:</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleSelectedStatus} placeholder="status" value={status} required type="text" id="status" name="status" className="form-control" />
                            <label htmlFor="status">Status</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleSelectedCustomer} placeholder="customer" value={customer} required type="text" id="customer" name="customer" className="form-control" />
                            <label htmlFor="customer">Customer</label>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="technician" className="form-label">Technician</label>
                            <select onChange={handleSelectedTechnician} required id="selectedTechnician" value={SelectedTechnician} name="selectedTechnician" className="form-select">
                                <option defaultValue="">Technician...</option>
                                {technician.map(tech => {
                                    return (
                                        <option key={tech.employee_id} value={tech.employee_id}>
                                            {tech.first_name} {tech.last_name}
                                        </option>
                                    )
                                })}
                            </select>
                        </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    )
}
export default CreateAppointment;