import React, { useEffect, useState } from 'react';

function CreateAutomobileForm() {
    const [color, setColor] = useState('');
    const [year, setYear] = useState('');
    const [vin, setVIN] = useState('');
    const [model, setModel] = useState([]);
    const [selectedModel, setSelectedModel] = useState('');

    const handleColorChange = (event) => {
        const value = event.target.value;
        setColor(value);
    }

    const handleYearChange = (event) => {
        const value = event.target.value;
        setYear(value);
    }

    const handleVINChange= (event) => {
        const value = event.target.value;
        setVIN(value);
    }

    const handleSelectedModelChange= (event) => {
        const value = event.target.value;
        setSelectedModel(value);
    }




    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.color = color;
        data.year = year;
        data.vin = vin;
        data.model_id = parseInt(selectedModel);

        const automobileURL = 'http://localhost:8100/api/automobiles/'
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(automobileURL, fetchConfig);
        if (response.ok) {
            const newAutomobile = await response.json();
            console.log(newAutomobile);

            setColor('');
            setYear('');
            setVIN('');
            setSelectedModel('');
        }
    }

    const fetchModels = async () => {
        const response = await fetch('http://localhost:8100/api/models/');

        if (response.ok) {
            const data = await response.json();
            setModel(data.models);
        }
    }

    useEffect(() => {
        fetchModels();
    }, []);

    return(
        <div className="container">
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Create an Automobile</h1>
                        <form onSubmit={handleSubmit} id="create-automobile-form">
                        <div className="form-floating mb-3">
                                <input onChange={handleColorChange} placeholder="color" value={color} required type="text" id="color" name="color" className="form-control" />
                                <label htmlFor="color">Color...</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={handleYearChange} placeholder="year" value={year} required type="text" id="year" name="year" className="form-control" />
                                <label htmlFor="year">Year...</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={handleVINChange} placeholder="vin" value={vin} required type="text" id="vin" name="vin" className="form-control" />
                                <label htmlFor="vin">VIN...</label>
                            </div>
                            <div className="mb-3">
                            <label htmlFor="selectedModel" className="form-label">Model</label>
                            <select onChange={handleSelectedModelChange} required id="selectedModel" value={selectedModel} name="selectedModel" className="form-select">
                                <option defaultValue="">Choose a model...</option>
                                {model.map(models => {
                                    return (
                                        <option key={models.id} value={models.id}>
                                            {models.name}
                                        </option>
                                    )
                                })}
                            </select>
                        </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    )
}


    export default CreateAutomobileForm;
