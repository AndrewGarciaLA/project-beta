import React, { useState, useEffect } from 'react';


function AppointmentList() {
    const [appointment, setAppointment] = useState([]);
    const [date, setDate] = useState('');
    const [time, setTime] = useState('');

    const fetchAppointments = async () => {
        const response = await fetch('http://localhost:8080/api/appointments/');

        if (response.ok) {
            const data = await response.json();
            setAppointment(data.appointment);
        }
    }
    useEffect(() => {
        fetchAppointments();
    }, []);
    
    const parseDateTime = (date_time) => {
        const [parseDate, parseTime] = date_time.split('T');
        date = parseDate
        time = parseTime
    }

    return (
        <>
            <h1>Appointment List</h1>
            <table className='table table-striped'>
                <thead>
                    <tr>
                        <th>VIN</th>
                        <th>Customer</th>
                        <th>Date</th>
                        <th>Time</th>
                        <th>Reason</th>
                        <th>Technician</th>
                    </tr>
                </thead>
                <tbody>
                    {appointment.map(app => {
                        return (
                            <tr key={app.id} value={app.id}>
                                <td>{app.vin}</td>
                                <td>{app.customer}</td>
                                <td>{app.date}</td>
                                <td>{app.time}</td>
                                <td>{app.reason}</td>
                                <td>{app.technician}</td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        </>
    )
}
export default AppointmentList;