import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import AppointmentList from './Appointments';
import Nav from './Nav';
import CreateSalespersonForm from './CreateSalespersonForm';
import ListSalesperson from './ListSalesperson';
import ListCustomer from './ListCustomer';
import CreateCustomerForm from './CreateCustomer';
import ListSales from './ListSales';
import CreateSalesForm from './CreateSalesForm';
import SalesHistory from './SalesHistory';
import ListManufacturers from './ListManufacturers';
import ListModels from './ListModels';
import ListAutomobiles from './ListAutomobile';
import CreateManufacturerForm from './CreateManufacturer';
import CreateModelForm from './CreateModel';
import CreateAutomobileForm from './CreateAutomobile';
import React, { useEffect, useState } from "react";
import CreateAppointment from './CreateAppointment';
import TechnicianList from './Technicians';
import CreateTechnician from './CreateTechnician';


function App() {

  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/customers/" element={<ListCustomer />} />
          <Route path="/customers/create" element={<CreateCustomerForm />} />
          <Route path="/salespeople/" element={<ListSalesperson />} />
          <Route path="/salespeople/create" element={<CreateSalespersonForm />} />
          <Route path="/sales/" element={<ListSales />} />
          <Route path="/sales/create" element={<CreateSalesForm />} />
          <Route path="/sales/history" element={<SalesHistory />} />
          <Route path="/manufacturers" element={<ListManufacturers />} />
          <Route path="/manufacturers/create" element={<CreateManufacturerForm />} />
          <Route path="/models" element={<ListModels />} />
          <Route path="/models/create" element={<CreateModelForm />} />
          <Route path="/automobiles" element={<ListAutomobiles />} />
          <Route path="/automobiles/create" element={<CreateAutomobileForm />} />
          <Route path='/appointments' element={<AppointmentList />} />
          <Route path="/appointments/create" element={<CreateAppointment />} />
          <Route path='/technicians' element={<TechnicianList />} />
          <Route path='/technicians/create' element={<CreateTechnician />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
