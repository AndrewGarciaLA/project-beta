import React, { useState, useEffect } from 'react';


function TechnicianList() {
    const [technicians, setTechnician] = useState([]);

    const fetchTechnician = async () => {
        const response = await fetch('http://localhost:8080/api/technicians/');

        if (response.ok) {
            const data = await response.json();
            setTechnician(data.technician);
        }

    }
    useEffect(() => {
        fetchTechnician();
    }, []);
    
    return (
        <>
            <h1>Technician List</h1>
            <table className='table table-striped'>
                <thead>
                    <tr>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Employee ID</th>
                    </tr>
                </thead>
                <tbody>
                    {technicians.map(tech => {
                        return (
                            <tr key={tech.id} value={tech.id}>
                                <td>{tech.first_name}</td>
                                <td>{tech.last_name}</td>
                                <td>{tech.employee_id}</td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        </>
    )
}
export default TechnicianList;