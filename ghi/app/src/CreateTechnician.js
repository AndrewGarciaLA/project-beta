import React, { useState, useEffect } from 'react';

function CreateTechnician() {
  const [first_name, setFirstName] = useState('');
  const [last_name, setLastName] = useState('');
  const [employee_id, setEmployeeId] = useState('');

  const handleFirstName = (event) => {
        const value = event.target.value;
        setFirstName(value);
    }

    const handleLastName = (event) => {
        const value = event.target.value;
        setLastName(value);
    }

    const handleEmployeeId = (event) => {
        const value = event.target.value;
        setEmployeeId(value);
    }


    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};

        data.first_name = first_name;
        data.last_name = last_name;
        data.employee_id = employee_id;


        const appointmentURL = 'http://localhost:8080/api/technicians/'
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
            const response = await fetch(appointmentURL, fetchConfig);
            if(response.ok){
                const newTechnician = await response.json();

                setFirstName('');
                setLastName('');
                setEmployeeId('');
            }
        }

    return(
        <div className="container">
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>New Appointment</h1>
                        <form onSubmit={handleSubmit} id="create-technician-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleFirstName} placeholder="First_Name" value={first_name} required type="text" id="First_Name" name="First_Name" className="form-control" />
                            <label htmlFor="First_Name">First Name:</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleLastName} placeholder="last_name" value={last_name} required type="text" id="last_name" name="last_name" className="form-control" />
                            <label htmlFor="last_name">last name:</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleEmployeeId} placeholder="employee_id" value={employee_id} required type="text" id="employee_id" name="employee_id" className="form-control" />
                            <label htmlFor="employee_id">employee id:</label>
                        </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    )
}
export default CreateTechnician;