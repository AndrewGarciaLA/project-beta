import React, { useEffect, useState } from 'react';

function CreateSalesForm() {
    const [vin, setVIN] = useState([]);
    const [selectedVIN, setSelectedVIN] = useState('');
    const [salesperson, setSalesperson] = useState([]);
    const [selectedSalesperson, setSelectedSalesperson] = useState('');
    const [customer, setCustomer] = useState([]);
    const [selectedCustomer, setSelectedCustomer] = useState('');
    const [price, setPrice] = useState('');
    const [automobileStatus, setAutomobileStatus] = useState(false);


    const handleSelectedVINChange = (event) => {
        const value = event.target.value;
        setSelectedVIN(value);
    }

    const handleSelectedSalespersonChange = (event) => {
        const value = event.target.value;
        setSelectedSalesperson(value);
    }

    const handleSelectedCustomerChange = (event) => {
        const value = event.target.value;
        setSelectedCustomer(value);
    }

    const handlePriceChange = (event) => {
        const value = event.target.value;
        setPrice(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};

        data.automobile = selectedVIN;
        data.salesperson = selectedSalesperson;
        data.customer = selectedCustomer;
        data.price = price;


        const salesURL = 'http://localhost:8090/api/sales/'
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(salesURL, fetchConfig);
        if (response.ok) {
            if (selectedVIN) {
                await fetch(`http://localhost:8100/api/automobiles/${selectedVIN}/`, {
                    method: 'put',
                    body: JSON.stringify({ sold: true }),
                    headers: {
                        'Content-Type': 'application/json',
                    },
                });
            }
            const newSale = await response.json();
            console.log(newSale);

            setSelectedVIN('');
            setSelectedSalesperson('');
            setSelectedCustomer('');
            setPrice('');

            fetchVIN();
        }
    }

    const fetchVIN = async () => {
        const response = await fetch('http://localhost:8100/api/automobiles/');

        if (response.ok) {
            const data = await response.json();
            setVIN(data.autos);
            if (data.autos.length > 0) {
                setAutomobileStatus(data.autos[0].sold);
            }
        }
    }

    const fetchSalesperson = async () => {
        const response = await fetch('http://localhost:8090/api/salespeople/');

        if (response.ok) {
            const data = await response.json();
            setSalesperson(data.salespeople);
        }
    }

    const fetchCustomer = async () => {
        const response = await fetch('http://localhost:8090/api/customers/');

        if (response.ok) {
            const data = await response.json();
            setCustomer(data.customers);
        }
    }

        useEffect(() => {
            fetchVIN();
            fetchSalesperson();
            fetchCustomer();

        }, []);

    const availableVINs = vin.filter(auto => !auto.sold);

    return(
        <div className="container">
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>New Sale</h1>
                        <form onSubmit={handleSubmit} id="create-sales-form">
                        <div className="mb-3">
                                <label htmlFor="vin" className="form-label">VIN</label>
                                <select onChange={handleSelectedVINChange} required id="selectedVIN" value={selectedVIN} name="selectedVIN" className="form-select">
                                    <option defaultValue="">Choose an automobile VIN...</option>
                                    {availableVINs.map(auto => {
                                        return (
                                            <option key={auto.vin} value={auto.vin}>
                                                {auto.vin}
                                            </option>
                                        )
                                    })}
                                </select>
                            </div>
                        <div className="mb-3">
                            <label htmlFor="vin" className="form-label">Salesperson</label>
                            <select onChange={handleSelectedSalespersonChange} required id="selectedSalesperson" value={selectedSalesperson} name="selectedSalesperson" className="form-select">
                                <option defaultValue="">Salesperson...</option>
                                {salesperson.map(sp => {
                                    return (
                                        <option key={sp.id} value={sp.id}>
                                            {sp.first_name} {sp.last_name}
                                        </option>
                                    )
                                })}
                            </select>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="vin" className="form-label">Customer</label>
                            <select onChange={handleSelectedCustomerChange} required id="selectedCustomer" value={selectedCustomer} name="selectedCustomer" className="form-select">
                                <option defaultValue="">Customer...</option>
                                {customer.map(custy => {
                                    return (
                                        <option key={custy.id} value={custy.id}>
                                            {custy.first_name} {custy.last_name}
                                        </option>
                                    )
                                })}
                            </select>
                        </div>
                            <div className="form-floating mb-3">
                                <input onChange={handlePriceChange} placeholder="price" value={price} required type="text" id="price" name="price" className="form-control" />
                                <label htmlFor="price">0</label>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    )
}


    export default CreateSalesForm;
