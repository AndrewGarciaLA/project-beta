import React, { useEffect, useState  } from 'react';


function ListModels() {
    const [models, setModels] = useState([]);


    const fetchModels = async () => {
        const response = await fetch("http://localhost:8100/api/models/")

        if (response.ok) {
            const data = await response.json();

            const sortedModels = [...data.models].sort((a, b) => {
                const nameA = a.name.toUpperCase();
                const nameB = b.name.toUpperCase();
                if (nameA < nameB) {
                    return -1;
                }
                if (nameA > nameB) {
                    return 1;
                }
                return 0;
            });
            setModels(sortedModels);
        }
    };


    useEffect(() => {
        fetchModels();
    }, []);


    return (
        <>
            <h1>Models</h1>
            <table className='table table-striped'>
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Manufacturer</th>
                        <th>Picture</th>
                    </tr>
                </thead>
                <tbody>
                    {models.map(model => {
                        return (
                            <tr key={model.id} value={model.href}>
                                <td>{model.name}</td>
                                <td>{model.manufacturer.name}</td>
                                <td>
                                {model.picture_url && (
                                    <img
                                        src={model.picture_url}
                                        alt={`Picture of ${model.name}`}
                                        style={{ maxWidth: '300px' }}
                                    />
                                )}
                            </td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        </>
    )
}

export default ListModels;
