import React, { useEffect, useState  } from 'react';


function ListCustomer() {
    const [customers, setCustomer] = useState([]);



    const fetchData = async () => {
        const response = await fetch("http://localhost:8090/api/customers/")

        if (response.ok) {
            const data = await response.json();

        const sortedCustomer = [...data.customers].sort((a, b) => {
            const nameA = a.first_name.toUpperCase();
            const nameB = b.first_name.toUpperCase();
            if (nameA < nameB) {
                return -1;
            }
            if (nameA > nameB) {
                return 1;
            }
            return 0;
        });
        setCustomer(sortedCustomer);
    }
};

    useEffect(() => {
        fetchData();
    }, []);


    return (
        <>
            <h1>Customers</h1>
            <table className='table table-striped'>
                <thead>
                    <tr>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Address</th>
                        <th>Phone Number</th>
                    </tr>
                </thead>
                <tbody>
                    {customers.map(custy => {
                        return (
                            <tr key={custy.id} value={custy.id}>
                                <td>{custy.first_name}</td>
                                <td>{custy.last_name}</td>
                                <td>{custy.address}</td>
                                <td>{custy.phone_number}</td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        </>
    )
}

export default ListCustomer;
