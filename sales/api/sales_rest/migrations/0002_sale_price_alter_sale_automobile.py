# Generated by Django 4.0.3 on 2023-09-05 19:53

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('sales_rest', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='sale',
            name='price',
            field=models.DecimalField(decimal_places=2, default=False, max_digits=10),
        ),
        migrations.AlterField(
            model_name='sale',
            name='automobile',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='car', to='sales_rest.automobilevo'),
        ),
    ]
