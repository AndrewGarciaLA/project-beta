from django.urls import path
from . import views

urlpatterns = [
    path("salespeople/", views.api_list_salesperson, name="api_list_salesperson"),
    path(
        "salespeople/<int:id>/",
        views.api_salesperson_detail,
        name="api_salesperson_detail",
    ),
    path("customers/", views.api_customer_list, name="api_customer_list"),
    path("customers/<int:id>/", views.api_customer_detail, name="api_customer_detail"),
    path("sales/", views.api_list_sales, name="api_list_sales"),
    path("sales/<int:id>/", views.api_sale_details, name="api_sale_details"),
]
