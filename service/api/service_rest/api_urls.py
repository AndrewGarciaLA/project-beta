from django.urls import path
from . import views


urlpatterns = [
    path('appointments/', views.api_list_appointments, name='api_list_appointments'),
    path('appointments/<int:appointment_id>/', views.api_appointment_detail, name='api_appointment_detail'),
    path('technicians/', views.api_list_technician, name='api_list_technicians'),
    path('technicians/<int:technician_id>/', views.api_technician_detail, name="api_technician_detail" ),
]