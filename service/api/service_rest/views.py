from django.shortcuts import render
from common.json import ModelEncoder
from .models import AutomobileVO, Appointment, Technician
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

# Create your views here.
class automobileVODetailEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        'id',
        'vin',
        'sold',
    ]

class technicianListEncoder(ModelEncoder):
    model = Technician
    properties = [
        'id',
        'first_name',
        'last_name',
        'employee_id',
    ]

class appointmentListEncoder(ModelEncoder):
    model = Appointment
    properties = [
        'id',
        'date_time',
        'reason',
        'status',
        'vin',
        'customer',
        'technician',
    ]
    def get_extra_data(self, o):
        return {'technician': o.technician.employee_id}

class appointmentDetailEncoder(ModelEncoder):
    model = Appointment
    properties = [
        'id',
        'date_time',
        'reason',
        'status',
        'vin',
        'customer',
        'technician',
    ]

    encoders = {
        'technician': technicianListEncoder(),
    }

class technicianDetailEncoder(ModelEncoder):
    model = Technician
    properties = [
        'id',
        'first_name',
        'last_name',
        'employee_id',
    ]

@require_http_methods(["GET", "POST"])
def api_list_appointments(request):
    if request.method == "GET":
        appointment = Appointment.objects.all()
        print("AAAAAAAAAA", appointment)
        return JsonResponse(
            {"appointment": appointment},
            encoder=appointmentListEncoder,
        )

    else:
        content = json.loads(request.body)

        # Get the Conference object and put it in the content dict
        try:
            appointment = content["technician"]
            technician = Technician.objects.get(id=appointment)
            content["technician"] = technician
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid technician id"},
                status=400,
            )


        appointment = Appointment.objects.create(**content)
        return JsonResponse(
            appointment,
            encoder=appointmentDetailEncoder,
            safe=False,
        )

    
@require_http_methods(["DELETE", "GET", "PUT"])
def api_appointment_detail(request, appointment_id):
    if request.method == "GET":
        details = Appointment.objects.get(id=appointment_id)
        return JsonResponse(
            details,
            encoder=appointmentDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Appointment.objects.filter(id=appointment_id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)

        Appointment.objects.filter(id=appointment_id).update(**content)

        appointment = Appointment.objects.get(id=appointment_id)
        return JsonResponse(
            appointment,
            encoder=appointmentDetailEncoder,
            safe=False,
        )

@require_http_methods(["GET", "POST"])
def api_list_technician(request):
    if request.method == "GET":
        technician = Technician.objects.all()
        return JsonResponse(
            {"technician": technician},
            encoder=technicianListEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            technician = Technician.objects.create(**content)
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Technician ID"},
                status=400,
            )
        return JsonResponse(
            technician,
            encoder=technicianListEncoder,
            safe=False,
        )
    
@require_http_methods(["DELETE", "GET", "PUT"])
def api_technician_detail(request, technician_id):
    if request.method == "GET":
        details = Technician.objects.get(id=technician_id)
        return JsonResponse(
            details,
            encoder=technicianDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Technician.objects.filter(id=technician_id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)

        Technician.objects.filter(id=technician_id).update(**content)

        technician = Technician.objects.get(id=technician_id)
        return JsonResponse(
            technician,
            encoder=appointmentDetailEncoder,
            safe=False,
        )