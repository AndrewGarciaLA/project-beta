from django.db import models

# Create your models here.
class Technician(models.Model):
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    employee_id = models.CharField(max_length=200)

class AutomobileVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True, default=False)
    vin = models.CharField(max_length=200)
    sold = models.BooleanField()

class Appointment(models.Model):
    date_time = models.DateTimeField()
    reason = models.CharField(max_length=200)
    status = models.CharField(max_length=200)
    vin = models.CharField(max_length=200)
    customer = models.CharField(max_length=200)
    technician = models.ForeignKey(
        Technician,
        related_name="tech",
        on_delete=models.CASCADE,
    )