import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "service_project.settings")
django.setup()

# Import models from service_rest, here. Ignore vs-code error hinting
# from service_rest.models import Something
from service_rest.models import AutomobileVO


def get_appointments():
    appointment_url = "http://service-api:8000/api/appointments/"
    response = requests.get(appointment_url)
    content = json.loads(response.content)
    print(content)

    for appointment in content["appointments"]:
        AutomobileVO.objects.update_or_create(
            import_href=appointment["href"],
            defaults={
                "vin": appointment["vin"],
                "sold": appointment["sold"],
            },
        )


def poll(repeat=True):
    while True:
        print('Service poller polling for data')
        try:
            get_appointments()
        except Exception as e:
            print(e, file=sys.stderr)

        if (not repeat):
            break

        time.sleep(60)


if __name__ == "__main__":
    poll()
