# CarCar

Team:

- Andrew G. - Sales
- Tyler C. - Service

## Design

## Service microservice

I created 3 models to take in all the information required by be project requirements. I created an automobileVO. I used the Technician and Appointment models to create the backend service of the application. I utilized these views to communicate this api data with other api's in the project. Now each appointment when made can have a unique technician assigned to the repair.

## Sales microservice

In the Sales microservice, I used 4 models. Using inventory microservice, I used the fields as needed to create my own AutomobileVO. This allows me to use the vin and sold status of the automobiles. The Salesperson, Customer, and Sale models I used in my views to create the backend. I made foreign (salesperson, automobileVo, and customer) in order to connect a sale to its customer, salesperson, and the unique vehicle.
